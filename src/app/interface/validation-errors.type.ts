export interface IValidationErrors {
    illegalPasswordFormat?: boolean;
    illegalFirstNameMatch?: boolean;
    illegalLastNameMatch?: boolean;
    invalidEmail?: boolean;
    required?: boolean;
}
