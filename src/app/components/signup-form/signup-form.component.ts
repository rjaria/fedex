import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  CustomvalidationService,
} from '../../services/validation/validation.service';
import { SignupService } from '../../services/signup/signup.service';
import { IValidationErrors } from '../../interface/validation-errors.type';
import { IUser } from 'src/app/interface/user.type';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})

export class SignupFormComponent implements OnInit {
  signupForm: FormGroup;
  submitted: boolean;
  submitResultSuccess: boolean;
  submitResultFail: boolean;
  isSubmitting: boolean;

  constructor(private formBuilder: FormBuilder,
              public customValidator: CustomvalidationService,
              public signupService: SignupService) {}

  ngOnInit() {
    this.submitted = false;
    this.isSubmitting = false;
    this.signupForm = this.formBuilder.group(
      {
        // no minimum length validation on the name fields because certain names
        // (i.e. south african xhosa names) might be just 1 letter
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        email: ['', Validators.compose([Validators.required, this.customValidator.emailValidator()])],
        password: ['', [Validators.required]]
      },
      {
        validator: this.customValidator.passWordValidator('password', 'firstName', 'lastName'),
      }
    );
  }

  // Getter function to get form controls value
  get f() {
    return this.signupForm.controls;
  }

  invalidFormControlValue(formControlName: string): boolean {
    return this.signupForm.get(formControlName).invalid && this.signupForm.get(formControlName).touched;
  }

  formControlErrors(formControlName: string): IValidationErrors {
    return this.signupForm.get(formControlName).errors;
  }

  createRequestBody(): IUser {
    return {
      firstName: this.signupForm.get('firstName').value,
      lastName: this.signupForm.get('lastName').value,
      email: this.signupForm.get('email').value
    };
  }

  onDismissAlert() {
    this.submitResultSuccess = false;
    this.submitResultFail = false;
  }

  onSubmit() {
    this.submitResultSuccess = false;
    this.submitResultFail = false;
    this.submitted = true;
    this.isSubmitting = true;
    // post user to signupService
    this.signupService.postUser(this.createRequestBody()).subscribe(
        result => {
          this.submitResultSuccess = true;
          this.submitResultFail = false;
          this.isSubmitting = false;
        },
        error => {
          // the endpoint always returned 200 status
          // (even if i sent a request body that was not according to specifications)
          // if it had  returned errors/errormessages in such a case then I
          // would have done something in the UI
          // (display the returned error message in stead of a generic error message)
          this.submitResultSuccess = false;
          this.submitResultFail = true;
          this.isSubmitting = false;
        });
  }
}
