import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  CustomvalidationService,
} from '../../services/validation/validation.service';
import { SignupService } from '../../services/signup/signup.service';
import { IUser } from 'src/app/interface/user.type';
import { SignupFormComponent } from './signup-form.component';
import { throwError, of } from 'rxjs';

describe('SignupFormComponent', () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;
  const mockUser: IUser = {
    firstName: 'Bruce',
    lastName: 'Wayne',
    email: 'iam@batman.dc'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupFormComponent ],
      providers: [
        FormBuilder,
        CustomvalidationService,
        {provide: SignupService, useValue: {
          postUser: () => of({subscribe: () => {
            return null;
          }})
    }},
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.submitted).toBeFalse();
    expect(component.signupForm).toBeDefined();
  });

  it('should return the form group', () => {
    expect(component.f).toBeDefined();
    expect(component.f.firstName).toBeDefined();
    expect(component.f.lastName).toBeDefined();
    expect(component.f.email).toBeDefined();
    expect(component.f.password).toBeDefined();
  });

  it('should return check if the entered value is valid', () => {
    component.signupForm = new FormBuilder().group(
      {
        email: ['abc', [Validators.minLength(4)]]
      }
    );
    component.signupForm.markAllAsTouched();
    expect(component.invalidFormControlValue('email')).toBeTrue();
    component.signupForm = new FormBuilder().group(
      {
        email: ['abcd', [Validators.minLength(4)]]
      }
    );
    expect(component.invalidFormControlValue('email')).toBeFalse();
  });

  it('should return the correct errors for a given formControl validation in case an invalid value was entered ', () => {
    component.signupForm = new FormBuilder().group(
      {
        email: ['abc', [component.customValidator.emailValidator()]]
      }
    );
    component.signupForm.markAllAsTouched();
    expect(component.formControlErrors('email')).toEqual({invalidEmail: true});


    component.signupForm = new FormBuilder().group(
      {
        firstName: ['bruce'],
        lastName: ['wayne'],
        email: ['batman@dc-comics.com'],
        password: ['wayne-bruce']
      },
      {
        validator: component.customValidator.passWordValidator('password', 'firstName', 'lastName'),
      }
    );
    component.signupForm.markAsTouched();
    expect(component.formControlErrors('password')).
    toEqual({illegalFirstNameMatch: true, illegalLastNameMatch: true, illegalPasswordFormat: true});

    component.signupForm = new FormBuilder().group(
      {
        firstName: ['bruce'],
        lastName: ['wayne'],
        email: ['batman@dc-comics.com'],
        password: ['bayne-bruce']
      },
      {
        validator: component.customValidator.passWordValidator('password', 'firstName', 'lastName'),
      }
    );
    component.signupForm.markAsTouched();
    expect(component.formControlErrors('password')).
    toEqual({illegalFirstNameMatch: true, illegalPasswordFormat: true});

    component.signupForm = new FormBuilder().group(
      {
        firstName: ['bruce'],
        lastName: ['wayne'],
        email: ['batman@dc-comics.com'],
        password: ['wayne-wruce']
      },
      {
        validator: component.customValidator.passWordValidator('password', 'firstName', 'lastName'),
      }
    );
    component.signupForm.markAsTouched();
    expect(component.formControlErrors('password')).
    toEqual({illegalLastNameMatch: true, illegalPasswordFormat: true});

    component.signupForm = new FormBuilder().group(
      {
        firstName: ['bruce'],
        lastName: ['wayne'],
        email: ['batman@dc-comics.com'],
        password: ['BruceWayne']
      },
      {
        validator: component.customValidator.passWordValidator('password', 'firstName', 'lastName'),
      }
    );
    component.signupForm.markAsTouched();
    expect(component.formControlErrors('password')).
    toEqual({illegalFirstNameMatch: true, illegalLastNameMatch: true});
  });

  it('should return null for a given formControl validation in case a  valid value was entered ', () => {
    component.signupForm = new FormBuilder().group(
      {
        email: ['batman@dc-comics.com', [component.customValidator.emailValidator()]]
      }
    );
    component.signupForm.markAllAsTouched();
    expect(component.formControlErrors('email')).toEqual(null);
  });

  it('should return a request body object based on the values entered in the form', () => {
    component.signupForm = new FormBuilder().group(
      {
        firstName: ['Bruce'],
        lastName: ['Wayne'],
        email: ['batman@dc-comics.com'],
        password: ['TheDarkKnightRises']
      }
    );
    component.signupForm.markAllAsTouched();
    expect(component.createRequestBody()).toEqual({firstName: 'Bruce', lastName: 'Wayne', email: 'batman@dc-comics.com'});
  });

  it('should set submitResultSuccess and submitResultFail to false', () => {
    component.submitResultSuccess = true;
    component.submitResultFail = true;
    component.onDismissAlert();
    expect(component.submitResultSuccess).toBeFalse();
    expect(component.submitResultFail).toBeFalse();
  });

  it('should set submitResultSuccess to true after succelfull POST to api endpoint', () => {
    component.submitResultSuccess = false;
    component.submitResultFail = false;
    component.signupForm = new FormBuilder().group(
      {
        firstName: ['Bruce'],
        lastName: ['Wayne'],
        email: ['batman@dc-comics.com'],
        password: ['TheDarkKnightRises']
      }
    );
    component.signupForm.markAllAsTouched();
    const mockUserPost: any = {
      succes: () => null
    };
    spyOn(component.signupService, 'postUser').and.returnValue(of(mockUserPost));
    component.onSubmit();
    expect(component.signupService.postUser).toHaveBeenCalledWith({
      firstName: 'Bruce',
      lastName: 'Wayne',
      email: 'batman@dc-comics.com'
    });
    expect(component.submitResultSuccess).toBeTrue();
    expect(component.submitResultFail).toBeFalse();
  });

  it('should set submitResultSuccess to true after unsuccelfull POST to api endpoint', () => {
    component.submitResultSuccess = false;
    component.submitResultFail = false;
    component.signupForm = new FormBuilder().group(
      {
        firstName: ['Bruce'],
        lastName: ['Wayne'],
        email: ['batman@dc-comics.com'],
        password: ['TheDarkKnightRises']
      }
    );
    component.signupForm.markAllAsTouched();
    spyOn(component.signupService, 'postUser').and.returnValue(throwError(new Error('test')));
    component.onSubmit();
    expect(component.signupService.postUser).toHaveBeenCalledWith({
      firstName: 'Bruce',
      lastName: 'Wayne',
      email: 'batman@dc-comics.com'
    });
    expect(component.submitResultSuccess).toBeFalse();
    expect(component.submitResultFail).toBeTrue();
  });

});
