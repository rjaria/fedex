import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { IUser } from './../../interface/user.type';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class SignupService {

  // Base url
  baseurl = 'https://demo-api.now.sh';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  postUser(data: IUser): Observable<IUser> {
    return this.http.post<IUser>(this.baseurl + '/users', data, this.httpOptions)
    .pipe(
      catchError(this.errorHandl)
    );
  }

  // Error handling
  errorHandl(error: HttpErrorResponse) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}, Message: ${error.message}`;
     }
     return throwError(errorMessage);
  }

}
