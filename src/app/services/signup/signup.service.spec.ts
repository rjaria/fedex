import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IUser } from './../../interface/user.type';
import { SignupService } from './signup.service';
import { throwError, of } from 'rxjs';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

describe('SignupService.postUser()', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let signupService: SignupService;

  beforeEach(() => {
    // Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SignupService
      ]
    });

    // Instantaites HttpClient, HttpTestingController and SignupService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    signupService = TestBed.inject(SignupService);
  });

  afterEach(() => {
    // Verifies that no requests are outstanding.
    httpTestingController.verify();
  });

  it('should post entered user info and return a response', () => {
    const newUser: IUser = { firstName: 'Clark', lastName: 'Kent', email: 'superman@dc-comics.com' };

    signupService.postUser(newUser).subscribe(
      data => expect(data).toEqual(newUser, 'should return 200 status and null'),
      fail
    );

    // postUser should have made one request to POST user
    const req = httpTestingController.expectOne(signupService.baseurl + '/users');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(newUser);

    // Expect server to return status 200 and null after POST
    const expectedResponse = new HttpResponse({ status: 200, statusText: 'Created', body: newUser });
    req.event(expectedResponse);
  });

  it('should turn 400 error into return of post of the user', () => {
    let response: any;
    let errResponse: any;
    const mockErrorResponse = { status: 400, statusText: 'Bad Request' };
    const data = 'Error Code: 400, Message: Http failure response for ' + signupService.baseurl + '/users: 400 Bad Request';
    const newUser: any = { firstName: 'Clark', lastName: 'Kent', email: 'clark@superman.dc' };

    signupService.postUser(newUser).subscribe(res => response = res, err => errResponse = err);
    httpTestingController.expectOne(signupService.baseurl + '/users').flush(data, mockErrorResponse);
    expect(errResponse).toBe(data);

  });

  it('should throw ErrorEvent in case doing the POST to the api was not possible (due to network issue)', () => {
    let response: any;
    const errResponse: any = {
        error: {
            message: 'errormessage from errorEvent'
        }
    };

    const mockErrorEvent: ErrorEvent = new ErrorEvent('test');
    const newUser: any = { firstName: 'Clark', lastName: 'Kent', email: 'clark@superman.dc' };
    const expectedResponse = new HttpResponse({ status: 200, statusText: 'Created', body: newUser });

    signupService.postUser(newUser).subscribe(res => response = res, err => errResponse);
    httpTestingController.expectOne(signupService.baseurl + '/users').error(mockErrorEvent);
  });
});
