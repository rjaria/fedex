import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomvalidationService {

  passWordValidator(password: string, firstName: string, lastName: string) {
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[password];
      const firstNameControl = formGroup.controls[firstName];
      const lastNameControl = formGroup.controls[lastName];

      /* It would have been a nice to have to check the entered password not as is,
       * but rather check a version of it that has all nonlatin characters converted
       * to latin characters. For instance a character with the german 'umlaut' would
       * then also be counted as a valid character.
       * Right now MöTORHEAD does not pass the password requirements but MoTORHEAD does.
       * the code here: http://semplicewebsites.com/removing-accents-javascript
       * could have madethat work. This was not implemented due to time constraints 
       * (mycurrent job/project is/was taking up all my time) */

      if (String(passwordControl.value) !== null &&
      String(passwordControl.value) !== '' &&
      (/[a-z]/.test(String(passwordControl.value)) &&
      /[A-Z]/.test(String(passwordControl.value))) !== true) {
          passwordControl.setErrors({ ...passwordControl.errors, illegalPasswordFormat: true });
      }

      if (String(passwordControl.value).length < 8) {
          passwordControl.setErrors({ ...passwordControl.errors, illegalPasswordFormat: true });
      }

      if (String(firstNameControl.value) !== null &&
          String(firstNameControl.value) !== '' &&
          String(passwordControl.value.toLowerCase()).includes(String(firstNameControl.value.toLowerCase()))) {
          passwordControl.setErrors({ ...passwordControl.errors, illegalFirstNameMatch: true });
      }
      if (String(lastNameControl.value) !== null &&
          String(lastNameControl.value) !== '' &&
          String(passwordControl.value.toLowerCase()).includes(String(lastNameControl.value.toLowerCase()))) {
          passwordControl.setErrors({ ...passwordControl.errors, illegalLastNameMatch: true });
      }
    };
  }

  emailValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const emailRegex = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');
      // prevent invalid entry like raoul@raoul.x from slipping trough
      const topLevelDomainExtension = control.value.substring(control.value.lastIndexOf('.'), control.value.length);
      // prevent invalid entry like raoul@raoul.co..uk from slipping trough
      const doubleDot = control.value.substring(control.value.lastIndexOf('.') - 1, control.value.lastIndexOf('.')) === '.';
      const isValidEmail = (emailRegex.test(String(control.value).toLowerCase()) && topLevelDomainExtension.length > 2 && !doubleDot);
      return (isValidEmail ? null : { invalidEmail: true });
    };
  }
}
