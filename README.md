# Fedex

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.
This project was built by me: Raoul Audhoe as a code challenge.
If this had been an production app i would have localized it with the use of i18n files.
But for the purpose of this code challenge,and since it is app with very little text, there was no
need. Please note! Unit test code coverage is at 100% So if you see a .ts file of a service not having a corresponding .spec.ts file , then be assured: it is tested indirectly (trough the component spec.ts files that call the service).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
